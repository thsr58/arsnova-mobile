/*
 * This file is part of ARSnova Mobile.
 * Copyright (C) 2011-2012 Christian Thomas Weber
 * Copyright (C) 2012-2015 The ARSnova Team
 *
 * ARSnova Mobile is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ARSnova Mobile is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ARSnova Mobile.  If not, see <http://www.gnu.org/licenses/>.
 */
Ext.define('ARSnova.view.home.NewMotdPanel', {
	extend: 'Ext.Panel',

	config: {
		fullscreen: true,
		scrollable: true,
		scroll: 'vertical'
	},

	sessionKey: null,

	/* toolbar items */
	toolbar: null,
	backButton: null,

	/*Editor Panel*/
	markdownEditPanel: null,
	subject: null,
	textarea: null,
	saveAndContinueButton: null,
	startTimeDatePicker: null,
	endTimeDatePicker: null,

	constructor: function (args) {
		this.callParent(arguments);	

		this.backButton = Ext.create('Ext.Button', {
			text: Messages.BACK,
			ui: 'back',
			handler: function () {
				var hTP = ARSnova.app.mainTabPanel.tabPanel.homeTabPanel;
				hTP.animateActiveItem(hTP.motdPanel, {
					type: 'slide',
					direction: 'right',
					duration: 700
				});
			}
		});

		this.toolbar = Ext.create('Ext.Toolbar', {
			title: Messages.NEW_MOTD,
			cls: 'titlePaddingLeft',
			docked: 'top',
			ui: 'light',
			items: [
				this.backButton
			]
		});

		this.markdownEditPanel = Ext.create('ARSnova.view.MarkDownEditorPanel', {
			processElement: this.textarea //text
		});

		this.subject = Ext.create('Ext.field.Text', {
			name: 'subject',
			placeHolder: Messages.CATEGORY_PLACEHOLDER
		});

		this.textarea = Ext.create('Ext.plugins.ResizableTextArea', {
			name: 'text',
			placeHolder: Messages.FORMAT_PLACEHOLDER
		});

		this.startTimeDatePicker = Ext.create('Ext.field.DatePicker', {
			name: 'startTimeDatePicker',
    		 	label: 'Message Startdate',
    			value: new Date(),
		});

		this.endTimeDatePicker = Ext.create('Ext.field.DatePicker', {
			name: 'endTimeDatePicker',
    		 	label: 'Message Enddate',
    		 	value: new Date(),
		});


		this.saveAndContinueButton = Ext.create('Ext.Button', {
			ui: 'confirm',
			cls: 'saveQuestionButton',
			text: Messages.SAVE_NEW_MESSAGE,
			style: 'margin-top: 70px',
			handler: function (button) {
				me.saveHandler(button);/*.then(function () {
					Ext.toast(Messages.QUESTION_SAVED, 3000);
				}).then(Ext.bind(function (response) {
					me.getScrollable().getScroller().scrollTo(0, 0, true);
				}, me));*/
			},
			scope: this
		});

		this.add([this.toolbar, {
			title: 'createMessage',
			style: {
				marginTop: '15px'
			},
			xtype: 'formpanel',
			scrollable: null,
			id: 'createMessage',
			submitOnAction: false,

			items: [{
				xtype: 'fieldset',
				items: [{
					xtype: 'fieldset',
					items: [this.subject]
				}, {
					xtype: 'fieldset',
					items: [this.markdownEditPanel, this.textarea]
				}, {
					xtype: 'fieldset',
					items: [this.startTimeDatePicker]
				},  {
					xtype: 'fieldset',
					items: [this.endTimeDatePicker]
				}]
			},this.saveAndContinueButton]
		}]);
	},

	saveHandler: function (button) {	 
		var mainPartValues = this.mainPart.getValues();
		var values = null;
		//values.text = mainPartValues.text;
		//values.subject = mainPartValues.subject;
	 
		console.log("save button clicked :", mainPartValues);
	}
	
});
